import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class TestAntraLogin {

    private static final String PROPERTIES_FILE = "login.properties";
    private static WebDriver driver;

    private String baseUrl = "http://esig-sandbox.ch/team2018_7/";
    private String login;
    private String mdp;

    @BeforeClass
    public static void prepareWebdriver() {

        // The config of 2018 is not working anymore in the current setting (org.openqa.selenium.WebDriverException: permission denied)
//        System.setProperty("webdriver.gecko.driver","C:\\ESIGUsers\\geckodriver-v0.23.0-win64\\geckodriver.exe");
//        // At ESIG, we need to use Firefox Developer Edition
//        System.setProperty("webdriver.firefox.bin","C:\\Program Files (x86)\\Firefox Developer Edition\\firefox.exe");
//        driver = new FirefoxDriver();

        System.setProperty("webdriver.chrome.driver", "C:\\ESIGUsers\\chromedriver_win32_v2.46\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Before
    public void readProperties() throws IOException {
        try (InputStream input = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {

            Properties prop = new Properties();
            prop.load(input);
            login = prop.getProperty("login");
            mdp = prop.getProperty("mdp");
        }
    }

    @AfterClass
    public static void closeWebdriver() {
        driver.close();
    }

    @Test
    public void employeLogin_happyPath() {
        driver.get(baseUrl + "connexion.php");
        driver.findElement(By.id("login")).sendKeys(login);
        driver.findElement(By.id("password")).sendKeys(mdp);
        driver.findElement(By.xpath("//button[text()='Se connnecter']")).click();
        assertEquals(baseUrl + "employe/saisieHoraire.php", driver.getCurrentUrl());
    }
}
